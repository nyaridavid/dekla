%% @type zsak_alak() = [zsak_elem()].
%% @type zsak_elem() = {any(),integer()}.
%% Az L lista zsák-alakja Zsak.

-module(khf1).
-author('nyaridavid@gmail.com').
-vsn('2015-09-26').
-export([lista_zsak/1]).
%-compile(export_all).

%% @spec khf1:elofordul(Elem::any(), L::list()) -> Count::integer() 
%% Eloadasdiakrol atveve
elofordul(Elem, L)
					-> length([X || X <- L, X=:=Elem]).


%% @spec khf1:lista_zsak(L::[any()]) -> Zsak::zsak_alak().
lista_zsak(L) 
					-> lista_zsak(L, []).


%% @spec khf1:lista_zsak(L::[any()], ResultAcc::zsak_alak()) -> Zsak::zsak_alak().
lista_zsak([], ResultAcc)
					-> ResultAcc;
lista_zsak([Item|Remainder], ResultAcc)
					-> 	FilterOutSameItems = fun (X) -> X =/= Item end,
						lista_zsak(lists:filter(FilterOutSameItems, Remainder),
								  ResultAcc ++ [{Item, 1 + elofordul(Item, Remainder)}]).  




