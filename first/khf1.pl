% :- type zsak_alak == list(zsak_elem).
% :- type zsak_elem ---> any - int.
% :- pred lista_zsak(list(any)::in, zsak_alak::out).
% lista_zsak(L, Zsak): Az L lista zsák-alakja Zsak.


% filternecount(E, L, Filtered, N) : Filtered az L lista E elemek nelkul, N = szurt elemek szama
filternecount(_, [], [], 0).
filternecount(E, [E|T], Filtered, N) :-
	filternecount(E, T, Filtered, NE),
	N is NE + 1.
filternecount(E, [H|T], [H | Rem], N) :-
	E \= H,
	filternecount(E, T, Rem, N).

% lista_zsak(L, Zsak): Az L lista zsák-alakja Zsak.
lista_zsak([], []).
lista_zsak([H|T], [-(H,Count)|Rem]) :- 
	filternecount(H, [H|T], FilteredList, Count),
	lista_zsak(FilteredList, Rem).	
