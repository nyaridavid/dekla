//#include <cekla.h>


//klistareal(A, B, N, KL) = kulonbozosegi lista ellenorzes ahol KL helyi ertekek szerint visszafele van
int klistareal(const int A, const int B, const int N, const list KL)
{
	const int bothZero = ((A == 0) * (B == 0));	

	if (KL != nil) {
		if (bothZero == 0) {	
			const int test = (A % N == B % N) ? 0 : 1;
			if (hd(KL) == test)
				return klistareal(A / N, B / N, N, tl(KL));
			else 
				return 0;
		} else {
			return 0;
		}
	} else {
		return bothZero;
	}
}


//revapp(L, L0) = az L lista megforditasa L0 ele fuzve
//forras = eloadasdia
list revapp(const list L, const list L0) {
	if (L == nil) return L0;
 	return revapp(tl(L), cons(hd(L), L0));
}


//reverse(L) = az L lista megforditva
//forras = eloadasdia
list reverse(const list L) {
	return revapp(L, nil);
}


//klista(A, B, N, KL) = feladat specifikacioja szerinti kulonbozosegi lista ellenorzes
int klista(const int A, const int B, const int N, const list KL)
{
	return klistareal(A, B, N, reverse(KL));
}


