% :- type zsak_alak == list(zsak_elem).
% :- type zsak_elem ---> any - int.
% :- pred lista_zsak(list(any)::in, zsak_alak::out).
% lista_zsak(L, Zsak): Az L lista zsák-alakja Zsak.

:- use_module(library(lists)).

% msort(L, Sorted) :- Sorted az L lista rendezve duplikatumok megorzesevel
msort(L, Sorted) :- keys_and_values(RawPairs, L, L),
	                keysort(RawPairs, OrdPairs),
					keys_and_values(OrdPairs, _, Sorted),
					!.


% lista_zsak_imp(L, Zsak, Count) :- L listabol Zsak alak, ahol az L lista elso elemenek darabszama Count
lista_zsak_imp([H], [-(H, Count)], Count) :- true, !.
lista_zsak_imp([H, H|TL], Zsak, Count) :-
					Var is Count + 1,
					lista_zsak_imp([H|TL], Zsak, Var),
					!.
lista_zsak_imp([H1, H2|TL], Zsak, Count) :-
						(H1 =\= H2 ->
							lista_zsak_imp([H2|TL], RevZsak, 1),
							Zsak = [-(H1, Count)|RevZsak]
						).

% lista_zsak(L, Zsak): Az L lista zsák-alakja Zsak alaklja amennyibel L lista elemei rendezhetoek
lista_zsak(L, Zsak) :- 	msort(L, SL),
						lista_zsak_imp(SL, Zsak, 1).

% valid_guess(GuessBag1, GuessBag2, WhiteCount) :- Meghatarozza, hogy ket tippbol kepzett zsak-bol WhiteCount alapjan illesztheto-e a ketto
valid_guess([], _, 0) :- true, !.
valid_guess(_, [], 0) :- true, !.
valid_guess([-(H1, LCount)|LT], [-(H2, RCount)|RT], White) :-
				( H1 =\= H2 ->
					( H1 < H2 ->
						valid_guess(LT, [-(H2, RCount)|RT], White)
					; H1 > H2 ->
						valid_guess([-(H1, LCount)|LT], RT, White)
					);
					select_min(DW, [LCount, RCount], _),
					NewWhite is White - DW,
					(0 =< NewWhite ->
						valid_guess(LT, RT, NewWhite)
					)
				).

% create_range(Start, End, L) - L egy lista Start kezdeti es End utolso elemmel ahol az elemek kozott a differencia 1
create_range(N, N, [N]).
create_range(Start, End, [Start|LT]) :-
			(Start =< End -> 
				Inc is Start + 1,
				create_range(Inc, End, LT)
			).

%search_valid_guess (PossibleDigits, RemainingDigits, Builder, Length, Code, White, K) :-
%Megvizsgalja egy uj tipp ervenyesseget ahol PossibleDigits a variacioban felhasznalhato szamjegyeket jeloli, a RemainingDigits a fuggveny altal meg iteralhato maradek szamjegyeket, a Builder az variacio aktualis allasat, Length a meg hatralevo variacio hosszat, Code az eredeti tipp zsak-ja, a White bedig az eredeti tippre kapott valasz, K pedig az uj ertelmes tipp ha van
search_valid_guess(PossibleDigits, [HDigit|TDigit], Builder, Length, Code, White, K) :-
				(Length > 0 -> (
					(
						NLength is Length - 1,
						search_valid_guess(PossibleDigits, PossibleDigits, [HDigit|Builder], NLength, Code, White, K)
					);(
						(TDigit \= [] ->
							search_valid_guess(PossibleDigits, TDigit, Builder, Length, Code, White, K)
						)
					)
					);(
						lista_zsak(Builder, Zsak),
						valid_guess(Zsak, Code, White),
						K = Builder
					)
				).				
	


% :- pred tipp_kod(int::in, simple_hint::in, code::out).
% tipp_kod(Max, Tipp-S, Kod): Kod a Max paraméternek és a Tipp-S párnak megfelel.
tipp_kod(MaxVal, -(Code, White), K) :- 
				lista_zsak(Code, CodeBag),
				length(Code, Length),
				create_range(1, MaxVal, Digits),
				search_valid_guess(Digits, Digits, [], Length, CodeBag, White, K). 
