%% @type zsak_alak() = [zsak_elem()].
%% @type zsak_elem() = {any(),integer()}.
%% Az L lista zsák-alakja Zsak.

%% @type code()          = [integer()].
%% @type simple_hint()   = {code(),integer()}

-module(khf2).
-author('nyaridavid@gmail.com').
-vsn('2015-10-16').
-export([tipp_kod/2]).
%-compile(export_all).

%% @spec khf2:lista_zsak(L::[any()]) -> Zsak::zsak_alak().
lista_zsak(L) 
					-> lista_zsak(lists:sort(fun (Lhs, Rhs) -> Lhs >= Rhs end, L), 1, []).


%% @spec khf2:lista_zsak(L::[any()], ResultAcc::zsak_alak()) -> Zsak::zsak_alak().
lista_zsak([H,H|Remainder], Counter, ResultAcc)
					-> 	lista_zsak([H|Remainder], Counter + 1, ResultAcc);  
lista_zsak([H1,H2|Remainder], Counter, ResultAcc)
					-> lista_zsak([H2|Remainder], 1, [{H1, Counter}|ResultAcc]);
lista_zsak([H|_], Counter, ResultAcc)
					-> [{H, Counter}|ResultAcc]. 

%% @spec khf2:subtract_bag(Lhs::zsak_alak(), Rhs::zsak_alak(), ResultAcc::integer()) -> Lhs es Rhs zsakokrol megmondja a kozos elemek szamat
subtract_bag([], _, ResultAcc)
					-> ResultAcc;
subtract_bag(_, [], ResultAcc)
					-> ResultAcc;
subtract_bag([{_EqualItem, LhsCount}|LhsBagTail], [{_EqualItem, RhsCount}|RhsBagTail], ResultAcc)
					-> subtract_bag(LhsBagTail, RhsBagTail, ResultAcc + lists:min([LhsCount, RhsCount]));
subtract_bag([{LhsItem, LhsCount}|LhsBagTail], [{RhsItem, RhsCount}|RhsBagTail], ResultAcc)
					-> case LhsItem < RhsItem of
					   		true -> subtract_bag(LhsBagTail, [{RhsItem, RhsCount}|RhsBagTail], ResultAcc);
					   		false -> subtract_bag([{LhsItem, LhsCount}|LhsBagTail], RhsBagTail, ResultAcc)
					   end.


%% @spec khf2:subtract_soreted_bag(LhsBag::zsak_alak(), RhsBag::zsak_alak()) -> Lhs es Rhs zsakokrol megmondja a kozos elemek szamat mar rendezett Zsakoknal
subtract_sorted_bag(LhsBag, RhsBag)
					-> subtract_bag(LhsBag, RhsBag, 0).

%% @spec is_valid_tip(TryCodeBag::zsak_alak(), CodeBag::zsak_alak(), WhiteCount::integer()) ->
%% megmondja egy uj kod (TryCodeBag) es elozo tipp (CodeBag) es a feher babu (WhiteCount) ismereteben, hogy ervenyes tipp-e.
is_valid_tip(TryCodeBag, CodeBag, WhiteCount) 
					-> subtract_sorted_bag(TryCodeBag, CodeBag) =:= WhiteCount.


%% @spec search_for_valid_tips (Depht::integer(), MaxVal::integer(), RemNum::[integer()], GuessAcc::[integer()],
%% 								SortedCodeBag::zsak_alak(), WhiteCount::integer()) -> [code()] az kigyujtott ertelmes tippeket adja meg
search_for_valid_tips(0, _, _, GuessAcc, SortedCodeBag, WhiteCount, ResultAcc)
					-> case is_valid_tip(lista_zsak(GuessAcc), SortedCodeBag, WhiteCount) of
					   		true -> [GuessAcc|ResultAcc];
					   		false -> ResultAcc
						end;
search_for_valid_tips(_, _, [], _, _, _, ResultAcc)
					-> ResultAcc;
search_for_valid_tips(Depht, AvailableDigits, [HNum|TNum], GuessAcc, SortedCodeBag, WhiteCount, ResultAcc)
					-> DephtResult = search_for_valid_tips(Depht - 1, AvailableDigits, AvailableDigits, [HNum|GuessAcc], SortedCodeBag, WhiteCount, ResultAcc),
					   search_for_valid_tips(Depht, AvailableDigits, TNum, GuessAcc, SortedCodeBag, WhiteCount, DephtResult).

%% @spec search_for_valid_tips (Length::integer(), MaxVal::integer(), SortedCodeBag::zsak_alak(), WhiteCount::integer()) -> [code()] az kigyujtott ertelmes tippeket adja meg
search_for_valid_tips(Length, MaxVal, SortedCodeBag, WhiteCount)
					-> search_for_valid_tips(Length, lists:seq(1, MaxVal), lists:seq(1, MaxVal), [], SortedCodeBag, WhiteCount, []).


%% @spec khf2:tipp_kod(Max::int(),Tipp_S::simple_hint()) -> Kodok::[code()].
%% @doc  A Max paraméternek és a Tipp_S párnak megfelelő kódok listája Kodok.
tipp_kod(MaxVal, {Code, WhiteCount}) when is_integer(MaxVal), MaxVal > 0, is_list(Code), is_integer(WhiteCount), WhiteCount >= 0
									 -> search_for_valid_tips(length(Code), MaxVal, lista_zsak(Code), WhiteCount).
