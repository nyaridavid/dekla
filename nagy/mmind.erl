%% @type code()          = [integer()|{'W', integer()}|{'B', integer()].
%% @type simple_hint()   = {code(),integer()}
%% @type approx()          = [code()].
%% @type optional_approx() = approx() | invalid_approx.
%% @type match_digit()	= 0 | 1.
%% @type match_seq()	= [match_digit()].
%% @type masked_guess_item() = {same | diff, integer()}.
%% @type masked_guess()		= [masked_guess_item()].
%% @type code()   = [integer()].
%% @type blacks() = integer().
%% @type whites() = integer().
%% @type answer() = {blacks(),whites()}.
%% @type hint()   = {code(),answer()}.

-module(mmind).
-author('nyaridavid@gmail.com').
-vsn('2015-11-07').
%-export([szukitese/2]).
-compile(export_all).

%% @spec member_list_gen_core(N::integer(), Black::integer(), Acc::match_seq(), ResultAcc::[match_seq()]) -> [match_seq()].
%% Segedfuggveny member_list_gen -hez. Leiras ott
member_list_gen_core(0, 0, Acc, ResultAcc) ->
	[Acc|ResultAcc];
member_list_gen_core(0, _, _, ResultAcc) ->
	ResultAcc;
member_list_gen_core(N, Black, Acc, ResultAcc) when N >= Black ->
	RES = member_list_gen_core(N-1, Black, [0|Acc], ResultAcc),
	if Black =/= 0	-> member_list_gen_core(N-1, Black-1, [1|Acc], RES);
	   true 		-> RES
	end;
member_list_gen_core(_, _, _, ResultAcc) ->
	ResultAcc.

%% @spec member_list_gen(N::integer(), Black::integer()) -> [match_seq()]
%% Feladata, hogy eloallitson olyan 0|1 sorozatot mely N hosszu, es Black db 1-es tartalmaz. Ezeket hasznaljuk egyezes vizsgalatra
member_list_gen(N, Black) -> member_list_gen_core(N, Black, [], []).



%% @spec mask(Mask::match_seq(), Guess::code()) -> masked_guess_item()
%% Egy 0|1 szekvenciahoz hozza rendeli, hogy az adott ponton egyezni kell a Tipp-el vagy elterni
mask(Mask, Guess) ->
	Zipped = lists:zip(Guess, Mask),
	lists:map(fun({G, M}) -> if M =:= 1 -> {same, G}; true -> {diff, G} end end, Zipped).



%% @spec check_match(MaskedGuess::masked_guess(), KM::approx()) -> approx() | fail
%% Ellenorzi, hogy az adott egyezosegekkel es az eredeti kozelito megoldasokkal milyen uj kozelito megoldasokat lehet eloallitani
check_match(MaskedGuess, KM) ->
	Zipped = lists:zip(KM, MaskedGuess),
	Mapped = lists:map(fun({L, I}) ->	case I of
										{diff, G}		   -> lists:filter(fun(Item) -> Item =/= G end, L); 
										{same, G}		   -> lists:filter(fun(Item) -> Item =:= G end, L)
										end 
					   end, Zipped),
	IsMemberMapped = lists:member([], Mapped),
	if IsMemberMapped -> fail; true -> Mapped end.



%% @spec merge(Lhs::[[integer()]], Rhs[[integer()]], ResultAcc::[[integer()]]) -> [[integer()]]
%% Listak listajat fuzi ossze olyan modon, hogy a belso listakat fuzi egymas utan
merge([], [], ResAcc) -> ResAcc;
merge([HLhs|TLhs], [HRhs|TRhs], ResAcc) ->
	merge(TLhs, TRhs, [(HLhs ++ HRhs)|ResAcc]).

%% @spec merge(Lhs::[[integer()]], Rhs[[integer()]]) -> [[integer()]]
%% Listak listajat fuzi ossze olyan modon, hogy a belso listakat fuzi egymas utan
merge(Lhs, Rhs) -> lists:reverse(merge(Lhs, Rhs, [])).



%% @spec empty_list_of_lists(N::integer()) -> [[]]
%% Letrehoz egy listat melyben van N db ures lista
empty_list_of_lists(N) -> lists:map(fun(_) -> [] end, lists:seq(1,N)).



%% @spec reduce(MatchSeqs::[match_seq()], KM::approx(), Guess::simple_guess(), Result::approx()) -> optional_approx()
%% Az egyezesi 0|1 szekvenciakbol, valamint az eredeti kozelito megoldasokbol es a Tipp-bol eloallitja a az uj kozelito megoldas listat
reduce([], _, _, Result) ->
	Filter = lists:filter(fun (X) -> X =/= [] end, Result),
	case Filter of
		[] -> invalid_approx;
		_	-> lists:map(fun (X) -> lists:usort(X) end, Result)
	end;
reduce([Mask|TMask], KM, Guess, Result) ->
	Masked = mask(Mask, Guess),
	case check_match(Masked, KM) of
		fail 	-> reduce(TMask, KM, Guess, Result);
		L		-> reduce(TMask, KM, Guess, merge(L, Result))
	end.



%% @spec khf3:szukitese(KM0::approx(),Tipp_P::simple_hint()) -> KM::optional_approx().
%% @doc  A KM közelítő megoldás a KM0 közelítésből a Tipp_P párra
%%       nézve felesleges értékek elhagyásával áll elő. Ha nincs
%%       érvényes megoldás, KM értéke az invalid_approx atom.
szukitese(KM, {Guess, BlackCount}) ->
	Length = length(Guess),
	reduce(member_list_gen(Length, BlackCount), KM, Guess, empty_list_of_lists(Length)).



%% @spec reduce_all_core(KM::approx(), Hints::[hint()]) -> KM::approx()
%% @doc Egy KM kozelito megoldast tovagg kozelit a megadott Hints tippek altal 
reduce_all_core(KM, []) -> KM;
reduce_all_core(KM, [{Code, {Black, _}}|THints]) ->
	reduce_all_core(szukitese(KM, {Code, Black}), THints).

%% @spec reduce_all(KM::approx(), MaxVal::int()]) -> KM::approx()
%% @doc Egy KM kozelito megoldast tovagg kozelit a megadott Hints tippek altal, ha 1-tol MaxVal
%% -ig terjedhetnek az ertekek
reduce_all(Hints, MaxVal) ->
	[{Code, {_,_}}|_] = Hints,
	Length = length(Code),
	reduce_all_core([lists:seq(1, MaxVal) || _ <- lists:seq(1, Length)], Hints).



%% @spec get_first_and_mark(Elem::any(), List::[any()], Mark::any()) -> List::[any()]
%% @doc Egy listaban megkeresi az elso megadott elemet, es ugy adja vissza a listat,
%% hogy az az elemet a Mark valtozoval egy Tuple-be foglalja
%% @throws 'fail' atomot dob hibakepp ha Elem nem volt talalhato a listaban 
get_first_and_mark(_, [], _) ->
	throw(fail);
get_first_and_mark(Elem, [Elem|T], Mark) ->
	[{Mark, Elem}|T];
get_first_and_mark(Elem, [H|T], Mark) ->
	[H|get_first_and_mark(Elem, T, Mark)].



%% @spec replace_in_index(N::int(), Value::any(), List::[any()]) -> List::[any()]
%% @doc A List N-edik elemet lecsereli Value-ra es ezzel a listaval ter vissza
replace_on_index(1, Value, [_|T]) ->
	[Value|T];
replace_on_index(N, Value, [H|T]) ->
	[H|replace_on_index(N-1, Value, T)].




%% @spec list_has_elem(Elem::any(), List::[any()] -> boolean()
%% @doc Ellenorzi, hogy List tartalmazza-e Elem-et
list_has_elem(_, []) ->
	false;
list_has_elem(Elem, [Elem|_]) ->
	true;
list_has_elem(Elem, [_|T]) ->
	list_has_elem(Elem, T).



%% @spec reduce_all_core(Digit::int(), DigitIndex::int(), Hints::[hint()], RemainingDigitNum::int) -> ReducedHints::[hint()]
%% @doc Szukiti a Hints Tipp-eket, az aktualis szamjegy es annak DigitIndex elhelyezkedese valamint az
%% alapjan, hogy meg hany szamjegyet kell megtalalni
reduce_hints(Digit, DigitIndex, Hints, RemainingDigitNum) ->
	Res = lists:map(fun (Hint) -> 
			  	{Code, {Black, White}} = Hint,
				if
					Black + White =< RemainingDigitNum ->	
						OnIndex = lists:nth(DigitIndex, Code),
						case OnIndex of
							{'W', Digit} -> 	
								if
									Black > 0 -> 
										try get_first_and_mark(Digit, Code, 'W') of
											NCode -> {replace_on_index(DigitIndex, {'B', Digit}, NCode), {Black - 1, White}}
										catch
											throw:fail -> {replace_on_index(DigitIndex, {'B', Digit}, Code), {Black - 1, White + 1}}
										end;
									Black =:= 0-> fail
								end;
							Digit ->
								if
									Black > 0 -> {replace_on_index(DigitIndex, {'B', Digit}, Code), {Black - 1, White}};
									Black =:= 0 -> fail
								end;
							_ -> 
								try get_first_and_mark(Digit, Code, 'W') of
									NCode -> {NCode, {Black, White - 1}}
								catch
									throw:fail -> {Code, {Black, White}}
								end
						end;
					true ->
						fail
				end
			  end, Hints),
	case list_has_elem(fail, Res) of
		true -> fail;
		false -> Res
	end.

%% @spec check_hints(ReducedHints::[hint()]) -> boolean()
%% @doc Ellenorzes, hogy a vegleg leszukitett tipp-ek megfelelnek-e a vegkriteriumnak
check_hints([]) -> 
	true;
check_hints([{_,{0,0}}|T]) ->
	check_hints(T);
check_hints(_) ->
	false.

solve_with_digit(Digit, TDigit, Hints, DigitIndex, RemainingDigitNum) ->
	case reduce_hints(Digit, DigitIndex, Hints, RemainingDigitNum) of
		fail -> [];
		RedHints -> [[Digit|X] || X <- solve_core(TDigit, RedHints, DigitIndex + 1, RemainingDigitNum - 1), X /= fail]
	end.

solve_core([], Hints, _, _) ->
	Res = check_hints(Hints),
	case Res of
		true -> [[]];
		false -> [fail]
	end;
solve_core([HDigit|TDigit], Hints, DigitIndex, RemainingDigitNum) ->
	lists:foldr(fun (Elem, Acc) -> 
					solve_with_digit(Elem, TDigit, Hints, DigitIndex, RemainingDigitNum) ++ Acc
				end, [], HDigit).

%% @spec mmind:mmind(Max::int(),Hints::[hint()]) -> Codes::[code()].
%% @doc  Codes a Hints súgás-listának és a Max maximális kódértéknek
%% megfelelő összes titkos kód tetszőleges sorrendű listája.
mmind(MaxVal, Hints) ->
	KM = reduce_all(Hints, MaxVal),
	solve_core(KM, Hints, 1, length(KM)).
