% :- type code     == list(int).
% :- type blacks   == int.
% :- type whites   == int.
% :- type approx        == list(list(int))
% :- type match_type  ---> 0|1.
% :- type match_digit ---> match_type()-int
% :- type match_list  ---> match_digit()-list(int)
% :- type answer ---> blacks/whites.
% :- type hint   ---> code-answer.
% :- type hints	 ---> list(hint).

:- use_module(library(lists)).
:- use_module(library(between)).
:- use_module(library(samsort)).

% :- match_list(int::in, int::in, list()::out).
% match_list(N, Black, MatchList) MatchList az N hosszu tippnel Black darabszamu fekete babu ismereteben egy lista ahol 1-es ahol egyezes lehet es 0-ahol nem
match_list(0, Black, []) :- !, Black == 0.
match_list(N, Black, [0|MatchListRet]) :-
	N1 is N - 1,
	N1 >= Black,
	match_list(N1, Black, MatchListRet).
match_list(N, Black, [1|MatchListRet]) :-
	N >= Black,
	N1 is N - 1,
	NBlack is Black - 1,
	match_list(N1, NBlack, MatchListRet).
	

% :- check_match(match_list()::in, approx()::out).
% check_match(MatchList, KM) MatchList alapjan eloallit egy lehetseges KM kozelito megoldast
check_match([], []).
check_match([GuessNum-KMI-0|T], [REM|KM]) :-
	(select(GuessNum, KMI, REM) ->
		true;
		REM = KMI),
	check_match(T, KM).
check_match([GuessNum-KMI-1|T], [REM|KM]) :-
	(member(GuessNum, KMI) -> 
		REM = [GuessNum];
		REM = []),
	check_match(T, KM).

% :- check_match_valid(match_list()::in, approx()::out).
% check_match_valid(MatchList, KM) MatchList alapjan egy lehetseges KM kozelito megoldas mely garantaltan ervenyes is
check_match_valid(L, KM) :-
	check_match(L, KM),
	\+ member([], KM).

% :- find_match(approx()::in, int::in, code()::in, int::in, approx()::out).
% find_match(KM0, N, Code, Black, KM) KM0 kozelito megoldas ismereteben, keres egyezeseket, a Code tipp annak N hossza es a Black fekete babuk szamaban alapjan KM-egy lehetseges ertekehez
find_match(KM0, N, Code, Black, KM) :-
	keys_and_values(CodeKM0, Code, KM0),
	match_list(N, Black, MatchList),
	keys_and_values(CodeKM0Match, CodeKM0, MatchList),
	check_match_valid(CodeKM0Match, KM).

% :- merge_list_of_lists_core(list(approx())::in, list(approx())::in, approx()::in, approx()::out)
% merge_list_of_lists_core(L1, L2, Acc, Merged) L1 es L2 listak listajat ossehozza belso listankent, duplikatumokat eltavolis es rendez. Acc a kezdolista, Merged pedig az osszefuzott lista
merge_list_of_lists_core([], [], []).
merge_list_of_lists_core([H1|T1], [H2|T2], [H12|Merged]) :-
	append(H1, H2, H12),
	merge_list_of_lists_core(T1, T2, Merged).

sorteriterator([H], SH) :- sort(H, SH), !.
sorteriterator([H|_], SH) :- sort(H, SH).
sorteriterator([_|T], SH) :- sorteriterator(T, SH).

% :- merge_list_of_lists_core(list(approx())::in, list(approx())::in, approx()::out)
% merge_list_of_lists_core(L1, L2, Merged) L1 es L2 listak listajat ossehozza belso listankent, duplikatumokat eltavolis es rendez. Merged pedig az osszefuzott lista
merge_list_of_lists(L1, L2, Merged) :-
	merge_list_of_lists_core(L1, L2, Appended),
	findall(SortIt, sorteriterator(Appended, SortIt), Merged).

% :- merge_endresults(list(approx())::in, approx()::out)
% merge_endresults(KMList, KM) - KMList kehetseges kozelito megoldasok listajat olvasztja ossze egy vegleges kozelito megoldasok listaba
merge_endresults([H|[]], H).
merge_endresults([H1,H2|T], KM) :-
	merge_list_of_lists(H1, H2, H12),
	merge_endresults([H12|T], KM).

% :- pred szukitese(approx::in, simple_hint::in, approx::out).
% szukitese(KM0, Tipp_P, KM): A KM érvényes közelítő megoldás a KM0
% közelítésből a Tipp_P párra nézve felesleges értékek elhagyásával áll elő.
szukitese(KM0, Code-Black, KM) :-
	length(Code, N),
	bagof(KM1, find_match(KM0, N, Code, Black, KM1), KMList),
	merge_endresults(KMList, KM).

% create_range(Start, End, L) - L egy lista Start kezdeti es End utolso elemmel ahol az elemek kozott a differencia 1
create_range(Start, End, Range) :-
	findall(VAL, between(Start, End, VAL), Range). 

% :- pred generate_initial_km(int::in, int::in, approx::out)
% generate_initial_KM(N, MaxVal, KM): KM egy kozelito megoldas, N elemmel es minden eleme 1-tol MaxVal-ig terjed
generate_initial_KM(N, MaxVal, KM) :-
	create_range(1, MaxVal, Range),
	findall(Range, between(1, N, _), KM).
			

% :- pred mmind(int::in, list(hint)::in, code::out).
% mmind(Max, Hints, Code): Code egy olyan titkos kód, amely megfelel a
%    a Hints súgás-listának és a Max maximális kódértéknek.		
reduceall(KM, [], KM):- !.
reduceall(KM, [Guess-(Black/_)|Rem], KMRes):-
	szukitese(KM, Guess-Black, KM1),
	reduceall(KM1, Rem, KMRes).
			
% :- cutselect(any::in, list(any)::in, list(any)::in)
% cutselect(Elem, List, Rem): A slect foggveny mely az elso megoldasa utan vagast vegez.
cutselect(Elem, List, Rem) :- select(Elem, List, Rem), !.

% :- pred clear_same(list(int)::in, list(int)::in, list(int)::out, list(int)::out, int::in, int::out).
% clear_same(L1I, L2I, L1O, L2O, Acc, N): L1O, L2O listak megegyeznek L1I, L2I listakkal kiveve ahol 
% ugyanazt az elemet tartalmaztak, azok kihagyasra kerulnek. N pedig az Acc-nak es az azonos elemek szamanak osszege
clear_same([], [], [], [], Acc, Acc) :- !.
clear_same([H|T1], [H|T2], R1, R2, Acc, N) :-
	NAcc is Acc + 1,
	clear_same(T1, T2, R1, R2, NAcc, N),
	!.
clear_same([H1|T1], [H2|T2], [H1|R1], [H2|R2], Acc, N) :-
	H1 \= H2,
	clear_same(T1, T2, R1, R2, Acc, N).

% :- check_core(code::in, code::in, whites::in)
% check_core(Guess, Code, White): Mar a feketektol megfosztott Guess es Code-nak White feherszamot kell produkalnia
check_core([], _, 0) :- !.
check_core([HGuess|TGuess], CodeBag, White) :-
	( cutselect(HGuess, CodeBag, CodeBagRem) ->
		White > 0,
		NWhite is White - 1,
		check_core(TGuess, CodeBagRem, NWhite)
	;
		check_core(TGuess, CodeBag, White)
	).

% :- check(code::in, hints::in).
% check(Guess, Hints): Ellenorzi, hogy a megadott kod, megfelel-e a Hints tippeknek
check(_, []).
check(Guess, [Code-Black/White|THints]) :-
	clear_same(Guess, Code, GuessC, CodeC, 0, Black),
	check_core(GuessC, CodeC, White),
	check(Guess, THints).

% :- solve_core(approx::in, code::in, code::in, whites::in, blacks::in, blacks::in, code::out).
% :- solve_core(KM, Code, CodeBag, White, Black, OrigBlack, Guess): KM egy szukitatt kozelito megoldas, Code es Codebag
% az aktualisan vizsgalt tipp, White a tipp fekete es feher osszegebol megmaradt resz, Black, az a meg megmarado fekete palcikak 
% szama
solve_core([], [], _, White, 0, OrigBlack, []) :-
	White =< OrigBlack.
solve_core([HDigit|TDigit], [HCode|TCode], CodeBag, White, Black, OrigBlack, [Digit|Acc]) :-
	member(Digit, HDigit),
	(Digit == HCode ->
		Black > 0,
		NBlack is Black - 1,
		solve_core(TDigit, TCode, CodeBag, White, NBlack, OrigBlack, Acc)
	; White > 0, cutselect(Digit, CodeBag, CodeBagRem) ->
		NWhite is White - 1,
		solve_core(TDigit, TCode, CodeBagRem, NWhite, Black, OrigBlack, Acc)
	;
		solve_core(TDigit, TCode, CodeBag, White, Black, OrigBlack, Acc)
	).	

% :- solve(approx::in, list(hint)::in, code::out)
% solve(Digits, Hints, K): Digist a helyiertekenkent elerheto szamjegzek listaja, Hints az eddigi tippek, a K pedig egy ervenyes megoldas a problemara
solve(Digits, Hints, K) :-
	[Code-Black/White|_] = Hints,
	BW is Black + White,
	solve_core(Digits, Code, Code, BW, Black, Black, K),
	check(K, Hints).

% :- prio_hint_keys(list(hint)::in, int::in, list(int)::out)
% prio_hint_keys(Hints, N, PrioHintKeys): A tippek fekete es feher syamabol egy prioritasi sorrendet ad meg,
% hogy melyik a legszigorubb es melyiket erdemes legelobb vizsalni, hogy a kerest elvaghassuk
prio_hints_keys([], _, []).
prio_hints_keys([_-(Black/White)|T], N, [Key|RET]) :-
	Key is (N * Black) + White,
	prio_hints_keys(T, N, RET).

% :- sort_hints(list(hint), N, list(hint))
% sort_hints(Hints, N, Sorted): Sorted a Hints tippek prioritas, (megkotesi erossegi) sorrendje, N pedig a tippek darabszama
sort_hints(Hints, N, Sorted) :-
	prio_hints_keys(Hints, N, HintsKeys),
	keys_and_values(HintPairs, HintsKeys, Hints),
	keysort(HintPairs, PrioHintPairs),
	keys_and_values(PrioHintPairs, _, PrioHintsRev),
	reverse(PrioHintsRev, Sorted),
	!.


% :- pred mmind(int::in, list(hint)::in, code::out).
% Code egy olyan titkos kód, amely megfelel a
%    a Hints súgás-listának és a Max maximális kódértéknek.
mmind(MaxVal, Hints, Code) :-
	[Hint-(_/_)|_] = Hints,
	length(Hint, N),
	generate_initial_KM(N, MaxVal, KM),
	reduceall(KM, Hints, KMRed),
	sort_hints(Hints, N, SortedHints),
	solve(KMRed, SortedHints, Code). 
