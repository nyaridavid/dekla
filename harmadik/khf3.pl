% :- type code          == list(int).
% :- type simple_hint ---> code-int.
% :- type approx        == list(list(int))
% :- type match_type  ---> 0|1.
% :- type match_digit ---> match_type()-int
% :- type match_list  ---> match_digit()-list(int)

:- use_module(library(lists)).

% :- pred sum_list(list()::in, int::out).
% sum_list(L, Sum) Sum az L lista elemeinek az osszege
sum_list([], 0).
sum_list([H|T], Sum) :-
	sum_list(T, SumTail),
	Sum is SumTail + H.

% :- variations(int::in, list()::in, list()::out).
% variations(N, Items, Variations) A Variations az N hosszu Items elemekbol letrehozott ismetleses variacio
variations(0, _, []).
variations(N, Items, [E|Variation]) :-
	N > 0,
	select(E, Items, _),
	N1 is N - 1,
	variations(N1, Items, Variation).

% :- match_list(int::in, int::in, list()::out).
% match_list(N, Black, MatchList) MatchList az N hosszu tippnel Black darabszamu fekete babu ismereteben egy lista ahol 1-es ahol egyezes lehet es 0-ahol nem
match_list(N, Black, MatchList) :-
	variations(N, [0,1], MatchList),
	sum_list(MatchList, Black).

% :- zip(list()::in, list()::in, list::out).
% zip(L1, L2, LOUT) LOUT egy olyan lista ahol L1 = [H1|_] es L2 = [H2|_] elemei egyenkent parba vannak allitva H1-H2 modon
zip([], [], []).
zip([H1|T1], [H2|T2], [H1-H2|REM]) :-
	zip(T1, T2, REM).
	
% :- check_match(match_list()::in, approx()::out).
% check_match(MatchList, KM) MatchList alapjan eloallit egy lehetseges KM kozelito megoldast
check_match([], []).
check_match([(0-GuessNum)-KMI|T], [REM|KM]) :-
	(select(GuessNum, KMI, REM) ->
		true;
		REM = KMI),
	check_match(T, KM).
check_match([(1-GuessNum)-KMI|T], [REM|KM]) :-
	(member(GuessNum, KMI) -> 
		REM = [GuessNum];
		REM = []),
	check_match(T, KM).

% :- check_match_valid(match_list()::in, approx()::out).
% check_match_valid(MatchList, KM) MatchList alapjan egy lehetseges KM kozelito megoldas mely garantaltan ervenyes is
check_match_valid(L, KM) :-
	check_match(L, KM),
	\+ member([], KM).

% :- find_match(approx()::in, int::in, code()::in, int::in, approx()::out).
% find_match(KM0, N, Code, Black, KM) KM0 kozelito megoldas ismereteben, keres egyezeseket, a Code tipp annak N hossza es a Black fekete babuk szamaban alapjan KM-egy lehetseges ertekehez
find_match(KM0, N, Code, Black, KM) :-
	match_list(N, Black, MatchList),
	zip(MatchList, Code, MatchList_Code),
	zip(MatchList_Code, KM0, MatchList_Code_KM0),
	check_match_valid(MatchList_Code_KM0, KM).

% :- merge_list_of_lists_core(list(approx())::in, list(approx())::in, approx()::in, approx()::out)
% merge_list_of_lists_core(L1, L2, Acc, Merged) L1 es L2 listak listajat ossehozza belso listankent, duplikatumokat eltavolis es rendez. Acc a kezdolista, Merged pedig az osszefuzott lista
merge_list_of_lists_core([], [], [], []).
merge_list_of_lists_core([], [], [H|T], [HS|Merged]) :-
	sort(H, HS),
	merge_list_of_lists_core([], [], T, Merged).
merge_list_of_lists_core([H1|T1], [H2|T2], Acc, Merged) :-
	append(H1, H2, H12),
	merge_list_of_lists_core(T1, T2, [H12|Acc], Merged).


% :- merge_list_of_lists_core(list(approx())::in, list(approx())::in, approx()::out)
% merge_list_of_lists_core(L1, L2, Merged) L1 es L2 listak listajat ossehozza belso listankent, duplikatumokat eltavolis es rendez. Merged pedig az osszefuzott lista
merge_list_of_lists(L1, L2, Merged) :-
	merge_list_of_lists_core(L1, L2, [], RevMerged),
	reverse(RevMerged, Merged).

% :- merge_endresults(list(approx())::in, approx()::out)
% merge_endresults(KMList, KM) - KMList kehetseges kozelito megoldasok listajat olvasztja ossze egy vegleges kozelito megoldasok listaba
merge_endresults([H|[]], H).
merge_endresults([H1,H2|T], KM) :-
	merge_list_of_lists(H1, H2, H12),
	merge_endresults([H12|T], KM).

% :- pred szukitese(approx::in, simple_hint::in, approx::out).
% szukitese(KM0, Tipp_P, KM): A KM érvényes közelítő megoldás a KM0
% közelítésből a Tipp_P párra nézve felesleges értékek elhagyásával áll elő.
szukitese(KM0, Code-Black, KM) :-
	length(Code, N),
	bagof(KM1, find_match(KM0, N, Code, Black, KM1), KMList),
	merge_endresults(KMList, KM).
