%% @type code()          = [integer()].
%% @type simple_hint()   = {code(),integer()}
%% @type approx()          = [code()].
%% @type optional_approx() = approx() | invalid_approx.
%% @type match_digit()	= 0 | 1.
%% @type match_seq()	= [match_digit()].
%% @type masked_guess_item() = {same | diff, integer()}.
%% @type masked_guess()		= [masked_guess_item()].

-module(khf3).
-author('nyaridavid@gmail.com').
-vsn('2015-10-16').
%-export([szukitese/2]).
-compile(export_all).

%% @spec member_list_gen_core(N::integer(), Black::integer(), Acc::match_seq(), ResultAcc::[match_seq()]) -> [match_seq()].
%% Segedfuggveny member_list_gen -hez. Leiras ott
member_list_gen_core(0, 0, Acc, ResultAcc) ->
	[Acc|ResultAcc];
member_list_gen_core(0, _, _, ResultAcc) ->
	ResultAcc;
member_list_gen_core(N, Black, Acc, ResultAcc) when N >= Black ->
	RES = member_list_gen_core(N-1, Black, [0|Acc], ResultAcc),
	if Black =/= 0	-> member_list_gen_core(N-1, Black-1, [1|Acc], RES);
	   true 		-> RES
	end;
member_list_gen_core(_, _, _, ResultAcc) ->
	ResultAcc.

%% @spec member_list_gen(N::integer(), Black::integer()) -> [match_seq()]
%% Feladata, hogy eloallitson olyan 0|1 sorozatot mely N hosszu, es Black db 1-es tartalmaz. Ezeket hasznaljuk egyezes vizsgalatra
member_list_gen(N, Black) -> member_list_gen_core(N, Black, [], []).



%% @spec mask(Mask::match_seq(), Guess::code()) -> masked_guess_item()
%% Egy 0|1 szekvenciahoz hozza rendeli, hogy az adott ponton egyezni kell a Tipp-el vagy elterni
mask(Mask, Guess) ->
	Zipped = lists:zip(Guess, Mask),
	lists:map(fun({G, M}) -> if M =:= 1 -> {same, G}; true -> {diff, G} end end, Zipped).



%% @spec check_match(MaskedGuess::masked_guess(), KM::approx()) -> approx() | fail
%% Ellenorzi, hogy az adott egyezosegekkel es az eredeti kozelito megoldasokkal milyen uj kozelito megoldasokat lehet eloallitani
check_match(MaskedGuess, KM) ->
	Zipped = lists:zip(KM, MaskedGuess),
	Mapped = lists:map(fun({L, I}) ->	case I of
										{diff, G}		   -> lists:filter(fun(Item) -> Item =/= G end, L); 
										{same, G}		   -> lists:filter(fun(Item) -> Item =:= G end, L)
										end 
					   end, Zipped),
	IsMemberMapped = lists:member([], Mapped),
	if IsMemberMapped -> fail; true -> Mapped end.



%% @spec merge(Lhs::[[integer()]], Rhs[[integer()]], ResultAcc::[[integer()]]) -> [[integer()]]
%% Listak listajat fuzi ossze olyan modon, hogy a belso listakat fuzi egymas utan
merge([], [], ResAcc) -> ResAcc;
merge([HLhs|TLhs], [HRhs|TRhs], ResAcc) ->
	merge(TLhs, TRhs, [(HLhs ++ HRhs)|ResAcc]).

%% @spec merge(Lhs::[[integer()]], Rhs[[integer()]]) -> [[integer()]]
%% Listak listajat fuzi ossze olyan modon, hogy a belso listakat fuzi egymas utan
merge(Lhs, Rhs) -> lists:reverse(merge(Lhs, Rhs, [])).



%% @spec empty_list_of_lists(N::integer()) -> [[]]
%% Letrehoz egy listat melyben van N db ures lista
empty_list_of_lists(N) -> lists:map(fun(_) -> [] end, lists:seq(1,N)).



%% @spec reduce(MatchSeqs::[match_seq()], KM::approx(), Guess::simple_guess(), Result::approx()) -> optional_approx()
%% Az egyezesi 0|1 szekvenciakbol, valamint az eredeti kozelito megoldasokbol es a Tipp-bol eloallitja a az uj kozelito megoldas listat
reduce([], _, _, Result) ->
	Filter = lists:filter(fun (X) -> X =/= [] end, Result),
	case Filter of
		[] -> invalid_approx;
		_	-> lists:map(fun (X) -> lists:usort(X) end, Result)
	end;
reduce([Mask|TMask], KM, Guess, Result) ->
	Masked = mask(Mask, Guess),
	case check_match(Masked, KM) of
		fail 	-> reduce(TMask, KM, Guess, Result);
		L		-> reduce(TMask, KM, Guess, merge(L, Result))
	end.



%% @spec khf3:szukitese(KM0::approx(),Tipp_P::simple_hint()) -> KM::optional_approx().
%% @doc  A KM közelítő megoldás a KM0 közelítésből a Tipp_P párra
%%       nézve felesleges értékek elhagyásával áll elő. Ha nincs
%%       érvényes megoldás, KM értéke az invalid_approx atom.
szukitese(KM, {Guess, BlackCount}) ->
	Length = length(Guess),
	reduce(member_list_gen(Length, BlackCount), KM, Guess, empty_list_of_lists(Length)).
		
