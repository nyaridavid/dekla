-module(kmmind).
-author('hanak@inf.bme.hu, kapolnai@iit.bme.hu, modified by: Andras Eisenberger, Balazs Galambosi').
-vsn('$LastChangedDate: 2015-10-28 22:28:31 +0100 (Wed, 28 Oct 2015) $$').

-compile(export_all).
-export([mmind_be/1,mmind_ki/2,megold/2,stopper/2,stopper/3, help/0,teljes_teszt/1,teszt/2]).

-import(mmind,[mmind/2]).

%% @spec kulon_node() -> boolean().
%% Igaz, ha külön node-on szeretnénk futtatni minden tesztesetet.
%% Előnye, hogy ha elszáll a számítás (pl. kevés memória), akkor folytatódik,
%% hátránya a problémásabb konfigurálás. Vagy epmd, vagy erl -sname indítás szükséges.
kulon_node() -> false.

%% @type code()   = [integer()].
%% @type blacks() = integer().
%% @type whites() = integer().
%% @type answer() = {blacks(),whites()}.
%% @type hint()   = {code(),answer()}.

%% @spec mmind:mmind(Max::int(),Hints::[hint()]) -> Codes::[code()].
%% @doc  Codes a Hints súgás-listának és a Max maximális kódértéknek
%%       megfelelő összes titkos kód tetszőleges sorrendű listája.

%% kmmind-függvények

%% @spec kmmind:mmind_be(FileIn::string()) -> MMind::{integer(),[hint()]}
%% @doc  MMind a FileIn szövegfájlból beolvasott Mmind-feladvány.
%%
mmind_be(FileIn) ->
    case file:open(FileIn,read) of
	{ok, InputFile} ->
	    Max = case io:get_line(InputFile,'') of
		      eof ->
			  io:format("Ures a(z) ~s fajl.~n",[FileIn]),
			  exit('Hibas feladvany');
		      I ->
			  string2integer(cleaned(I))
		  end,
	    Hints = leiro_be(InputFile),
	    file:close(InputFile),
	    case helyes_feladvany(Max,Hints) of
		true ->
		    {Max,Hints};
		false ->
		    io:format("Hibas a feladvany a(z) ~s fajlban.~n",[FileIn]),
		    exit('Hibas feladvany')
	    end;
	_ ->
	    io:format("Nincs ~s nevu fajl.~n",[FileIn]),
	    exit('Nincs ilyen feladvany')
    end.

leiro_be(InputFile) ->
    leiro_be(InputFile,[]).

leiro_be(InputFile,Zss) ->
    case io:get_line(InputFile,'') of
	eof ->
	    lists:reverse(Zss);
	Sor ->
	    Xs = string:tokens(cleaned(Sor)," "),
	    Cs = lists:map(fun string2integer/1,lists:sublist(Xs,length(Xs)-1)),
	    Ys = string:tokens(lists:last(Xs),"/"),
	    [B,W] = lists:map(fun string2integer/1, Ys),
	    case Cs of
		[] ->
		    leiro_be(InputFile,Zss);
		_ ->
		    leiro_be(InputFile,[{Cs,{B,W}}|Zss])
	    end
    end.

cleaned(Sor) ->
    S0 = lists:filter(fun(C) -> C /= $\t end,Sor),
    string:strip(string:strip(S0),right,$\n).

string2integer(S) ->
    element(1,string:to_integer(S)).

%% @spec helyes_feladvany(Max::int(),Hints::[hint()]) -> -> B::bool().
%% @doc  B igaz, ha a feladvány megfelel a specifikációnak.
%%
helyes_feladvany(Max,Hints) ->
    Szinhiba = "A tippekben hibas szinkod van (1 es ~w kozott lehet).~n",
    Tipphiba = "Nem egyforma hosszuak a tippek.~n",
    Valaszhiba = "A valaszok szama hibas (0 es ~w kozott lehet).~n",
    {Ts,As} = lists:unzip(Hints),
    H = length(hd(Ts)),
    Tippteszt = fun(X) -> length(X) == H end,
    Id = fun(X) -> X end,
    Valaszteszt = fun({X,Y}) -> XY = X+Y, 0 =< XY andalso XY =< H end,
    Cs = lists:concat(Ts),
    case
	Max < 1 of
	true ->
	    io:format("A szinek szama ~w; 1-nel nem lehet kisebb.~n",[Max]),
	    false;
	false ->
	    case
		lists:all(Id, lists:map(Tippteszt,Ts)) of
		false ->
		    io:format(Tipphiba),
		    false;
		true ->
		    case
			lists:foldl(fun erlang:min/2, 1, Cs) < 1 orelse
			lists:foldl(fun erlang:max/2, Max, Cs) > Max of
			true ->
			    io:format(Szinhiba,[Max]),
			    false;
			false ->
			    case
				lists:all(Id, lists:map(Valaszteszt,As)) of
				false ->
				    io:format(Valaszhiba,[H]),
				    false;
				true ->
				    true
			    end
		    end
	    end
    end.

%% @spec kmmind:mmind_ki(FileOut::string(),Codes::[code()]) -> void()
%% @doc  A Codes listában átadott Mmind-megoldásokat kiírja
%%       a FileOut szövegfájlba.
%%
mmind_ki(FileOut,SSols) ->
    {ok,OutputFile} = file:open(FileOut,write),
    io:fwrite(OutputFile,"~w.~n",[SSols]),
    io:nl(OutputFile),
    file:close(OutputFile).

%% @spec mmindsol_be(FileIn::string()) -> Sol::[code()].
%% @doc  Sol a FileIn szövegfájlból beolvasott Mmind-megoldás.
mmindsol_be(FileSol) ->
    case file:open(FileSol,read) of
        {ok, InputFile}      -> ok;
        {error, InputFile=R} -> throw({error,R,file:format_error(R)})
    end,
    case io:read(InputFile, "") of
        {ok, Sol}       -> ok;
        {error, Sol=R2} -> throw({error,R2,file:format_error(R2)})
    end,
    file:close(InputFile),
    Sol.

%% @spec kmmind:megold(FileIn::string(),FileOut::string()) -> void()
%% @doc  Beolvas egy feladványt a FileIn szövegfájlból és összes
%%       megoldását kiírja a FileOut szövegfájlba. Ehhez
%%       felhasználja a mmind:mmind/2 függvényt.
%%
megold(FileIn,FileOut) ->
    {Max,Hints} = mmind_be(FileIn),
    Codes = mmind:mmind(Max,Hints),
    mmind_ki(FileOut,Codes).

%% @spec stopper(FileIn::string(),FileOut::string()) -> void()
%% @doc  Mint megold/2, de a végén kiírja a FileIn nevet,
%%       a megoldások számát és a futási idõt is.
stopper(FileIn,FileOut) -> stopper(FileIn,FileOut,"").

%% @spec stopper(FileIn::string(),FileOut::string(),FileSol::string()) -> void()
%% @doc  Mint megold/2, de a végén kiírja a FileIn nevet,
%%       a megoldások számát és a futási idõt is. Ha lehet, összeveti FileSol megoldással is.
stopper(FileIn,FileOut,FileSol) ->
    {Max,Hints} = mmind_be(FileIn),
    erlang:garbage_collect(),timer:sleep(10),
    _T1 = statistics(runtime),
    Codes = mmind:mmind(Max,Hints),
    {_,T2} = statistics(runtime),
    mmind_ki(FileOut,lists:sort(Codes)),
    receive _ -> void after 1 -> void end, % ha pont itt lenne Timeout, akkor cancel % TODO uzenni, h most mar ne legyen timeout
    io:format("Feladvany: ~s, ", [FileIn]),
    try
        case lists:sort(mmindsol_be(FileSol)) =:= lists:sort(Codes) of
            true  -> io:format("HELYES ");
            false -> io:format("ROSSZ ")
        end
    catch
        {error, _, _} -> error%io:format("NINCS MO ")
    end,
    io:format("megoldas: ~3w, futasi ido: ",
	      [length(Codes)]),
    if T2 >= 10000 -> io:format("~5w s\n", [ T2 div 1000 ]);
%       T2 >= 1000 -> io:format("~5.2f s\n", [ T2 / 1000 ]);
       true        -> io:format("~5w ms\n", [ T2 ])
    end.


%% ------- experimental -----


help() ->
    io:format(
      "teljes_teszt(Timeout): meg kiserleti stadiumban van."
      "Megoldja az osszes, tests/testXXXd.txt specifikaciot idolimittel.~n"
      "Pelda: futtatas bash-bol, 10s idolimit, megoldasok tests/testXXXt.txt fajlokba: ~n"
      "$ erl -noshell -eval 'kmmind:teljes_teszt(10).' -s init stop~n"
     ).

%% @spec teljes_teszt(Timeout::integer()) -> [done|timeout].
%% @doc A 'tests' könyvtárban levõ összes "testXXXd.txt" tesztállomány esetén
%%  - lefuttatja a tesztet Timeout másodperces idõkorláttal,
%%  - ellenõrzi, hogy a testXXXs.txt állományban megadott megoldáshalmazt kapta,
%%  - olvasható formában (lásd megold/2) kiírja az eredményt a 'tests_out_er'
%%    könyvtár testXXXt.txt nevû állományába.
%% Az állománynevekben az XXX szám tetszõleges hosszúságú lehet.
teljes_teszt(Timeout) ->
    Dir = "tests/",
    ODir = "tests_out_er/",
    case file:make_dir(ODir) of
        ok              -> ok;
        {error, eexist} -> ok;
        {error, Rd}     -> throw({error,Rd,file:format_error(Rd)})
    end,
    case file:list_dir(Dir) of
        {error, R} ->
            throw({error,R,file:format_error(R)});
        {ok, Content} ->
            IOFiles = [{Dir++Input,ODir++Output,Dir++Solution}
                       || Input <- lists:sort(Content),
                          {match,_} <- [re:run(Input, "test[0-9]*d\.txt")],
                          Solution <- [re:replace(Input, "d\.txt\$", "s.txt", [{return, list}])],
                          Output <- [re:replace(Input, "d\.txt\$", "t.txt", [{return, list}])]
                      ],
            teszt(Timeout, IOFiles)
    end.

%% @spec teszt(Timeout::integer(),
%%            IOFiles::[{Input::string(),Output::string(),Solution::string()}]) -> [done|timeout].
%% @doc Megoldja az összes Input fájlban található specifikációt Timeout idõlimittel,
%%      megoldásokat az Output fájlokba teszi. Ha lehet, összeveti a megoldásokat Solution-nel.
teszt(Timeout, IOFiles) ->
    kulon_node() andalso io:write(net_kernel:start([masternode,shortnames])), % legyen neve a node-nak
    timer:sleep(100),
    [begin
         case kulon_node() of
             true -> % külön node a biztonság kedvéért (memória elfogy)
                 {ok, WN} = slave:start(element(2, inet:gethostname()), workernode),
                 timer:sleep(100); % különben Crash {"init terminating in do_boot",not_alive}
             false -> WN = void
         end,
         process_flag(trap_exit, true),
         Worker = case kulon_node() of
             true -> spawn_link(WN, fun() -> stopper(Input, Output, Solution) end);
             false -> spawn_link(fun() -> stopper(Input, Output, Solution) end)
         end,
         Timer  = spawn_link(fun() -> timer:sleep(Timeout * 1000) end),
         receive
             {'EXIT', Worker, _Reason=normal} -> 
                 Peer=Timer,
                 Result=done;
             {'EXIT', Worker, _Reason} -> 
                 io:format("Feladvany: ~s -- Crash (keves memoria?)~n", [Input]),
                 Peer=Timer,
                 Result=crash;
             {'EXIT', Timer, _Reason} ->
                 io:format("Feladvany: ~s -- Timeout (~p s)~n", [Input,Timeout]),
                 Peer=Worker,
                 Result=timeout
         end,
         exit(Peer, kill),
         receive {'EXIT', Peer, killed} -> ok end, % feldolgozzuk a halalat
         slave:stop(WN),
         Result
     end
     || {Input, Output, Solution} <- IOFiles
    ].
