% $LastChangedDate: 2015-10-28 09:58:33 +0100 (Wed, 28 Oct 2015) $
% :- type file      == atom.

% :- type code     == list(int).
% :- type blacks   == int.
% :- type whites   == int.
% :- type answer ---> blacks/whites.
% :- type hint   ---> code-answer.

% :- pred mmind(int::in, list(hint)::in, code::out).
% mmind(Max, Hints, Code): Code egy olyan titkos k�d, amely megfelel a
%    a Hints s�g�s-list�nak �s a Max maxim�lis k�d�rt�knek.

:- module(kmmind,
	  [
	   mmind_be/3,	    % mmind_be(file::in, int::out, list(hint)::out)
	   mmind_ki/2,	    % mmind_ki(file::in, list(code)::in)
	   megold/2,	    % megold(file::in, file::in)
	   stopper/3,	    % stopper(file::in, file::in)
	   teljes_teszt/1   % teljes_teszt(integer::in)
	  ]
	 ).

:- use_module(library(samsort)).

%-------------- Bejaratok ------------------------------------

% mmind_be(+File, -Colours, -Hints):
% A File-bol beolvashato egy Colours db. szint hasznalo, Hints sugasokat
% megado feladat.
mmind_be(File, Colours, Hints) :-
	open_file(File, read, Stream),
	read_in(Stream, Characters),
	close(Stream),
	parse_data(Characters, Colours, Hints).

% mmind_ki(+File, +Solution):
% A Solution megoldast kiirja a File allomanyba.
mmind_ki(File, Solution) :-
	open_file(File, write, Stream),
	current_output(OOut),
	set_output(Stream),
	call_cleanup(print_int_line(Solution),
		     (set_output(OOut),close(Stream))).

% megold(+FileIn, +FileOut):
% A FileIn allomanybol beolvasott feladat minden megoldasat
% kiirja a FileOut allomanyba.
megold(FileIn, FileOut) :-
	megold(FileIn, FileOut, _S, _T).

% stopper(+FileIn, +FileOut, -Solutions):
% A FileIn allomanybol beolvasott feladat minden megoldasat kiirja a
% FileOut allomanyba. A szabvanyos kimenetre kiirja a futasi idot es a
% megoldasok szamat. Solutions a megoldasok listaja.
stopper(FileIn, FileOut, Solutions) :-
	megold(FileIn, FileOut, Solutions, T),
	length(Solutions, N),
	format('Feladvany: ~a, megoldas: ~|~t~d~4+, futasi ido: ~|~t~3d~8+ s', [FileIn,N,T]).

% megold(+FileIn, +FileOut, -S, -T):
% A FileIn allomanybol beolvasott feladat minden megoldasat
% kiirja a FileOut allomanyba. A megoldasok listaja S, a futasi ido T msec.
megold(FileIn, FileOut, Solutions, T) :-
	mmind_be(FileIn, Colours, Hints),
	ensure_loaded(user:mmind),
	statistics(runtime, [T0|_]),
	megoldasok(Colours, Hints, Solutions),
	statistics(runtime, [T1|_]),
	T is T1 - T0,
	open_file(FileOut, write, Stream),
	current_output(OOut),
	set_output(Stream),
	call_cleanup(format('~p.',[Solutions]), %print_solutions(Solutions),
		     (set_output(OOut),close(Stream))).

% megoldasok(+Colours, +Hints, -Solutions):
% A Colours es Hints jellemzoju feladat osszes megoldasa
% a Solutions rendezett lista.
megoldasok(Colours, Hints, Solutions) :-
	findall(Solution,
		user:mmind(Colours, Hints, Solution),
		Solutions0),
	samsort(Solutions0, Solutions).

% A Solutions megoldaslista minden elemet
% kiirja az aktualis kimenetre.
print_solutions(Solutions) :-
	member(Solution, Solutions),
	print_int_line(Solution),
	fail.
print_solutions(_).

% print_int_line(+L):
% Egy-egy szokozzel elvalasztva kirja L elemeit es uj sort az aktualis
% kimenetre.
print_int_line([]) :-
	nl.
print_int_line([H|T]) :-
	write(H), write(' '),
	print_int_line(T).

%-------------- Allomanyok kezelese --------------------------

% open_file(+File, +Mode, -Stream):
% A File allomanyt megnyitja a Stream folyam formajaban Mode modban.
open_file(user, M, S) :-
	!, standard_stream(M, S).
open_file(File, Mode, Stream) :-
	open(File, Mode, Stream).

% standard_stream(?Mode, ?Stream):
% A Mode modu szabvanyos folyam a Stream.
standard_stream(read, user_input).
standard_stream(write, user_output).

%-------------- Beolvasas ------------------------------------

% read_in(+S, -L):
% S folyambol beolvashato karakterek listaja L.
read_in(S, L) :-
	get_code(S, C),
	read_in(C, S, L).

% read_in(+C, +S, -L):
% Ha C az S folyambol elozoleg beolvasott karakter, akkor L az S-bol
% beolvashato maradek karakterek listaja, C-t is beleertve.  Ha C = -1,
% akkor L = [].
read_in(C, _, L) :-
	C =:= -1, !, L = [].
read_in(C, S, [C|L]) :-
	get_code(S, C1),
	read_in(C1, S, L).

% parse_data(+Chars, -Colours, -Hints):
% Chars fuzerbol kielemezheto egy Colours db. szint hasznalo Hints sugasokat
% megado feladat.
parse_data(Chars, Colours, Hints) :-
	first_int(Colours, Chars, Chars1),
	int_lists(IntLists, Chars1, Rest),
	correct_syntax(Rest),
	int_lists_hints(IntLists, Hints).

% int_lists_hints(+IL, -HL):
% Az IL egeszek listajanak listaja a HL sugasok listajat adja.
int_lists_hints([], []).
int_lists_hints([IL|TIL], [Code-Blacks/Whites|TH]) :-
	append(Code, [Blacks,Whites], IL),
	int_lists_hints(TIL, TH).

%-------------- Ellenorzesek ---------------------------------

% correct_syntax(+Rest):
% A Rest fuzer a bemenet helyes maradeka, azaz ures.
correct_syntax(Rest) :-
	Rest = [_|_], !,
	chars_of_type(mid_line, L, Rest, _),
	warning('non-digit character in line: "~s"', [L]),
	fail.
correct_syntax(_).

%-------------- DCG elemzes ----------------------------------

% first_int(-Int) -->
% Kielemezheto az Int egesz, amit esetleg nem lathato karakterek
% eloznek meg.
first_int(Int) -->
	chars_of_type(layout, _),
	int_number(Int).

% int_lists(-LL) -->
% Kielemezheto az LL egeszek listainak listaja.
int_lists([IntList|IntLists]) -->
	chars_of_type(layout, _),
	int_list(IntList), !,
	int_lists(IntLists).
int_lists([]) --> [].

% int_list(-IntList) -->
% Kielemezheto az IntList egeszek listaja, ujsorral lezarva.
% Az egeszeket vizszintes ures karakterek valasztjak el es egy
% `/' karakter is elofordulhat kozottuk.
% A bemenet nem kezdodhet nem lathato karakterrel.
int_list([Int|IntList]) -->
	int_number(Int), !,
	chars_of_type(horiz_sp, _),
	opt_slash,
	chars_of_type(horiz_sp, _),
	int_list(IntList).
int_list([]) -->
	char_of_type(vert_sp, _).

% int_number(-Int) -->
% Kielemezheto egy (tizes szamrendszerbeli) Int nem-negativ egesz szam.
int_number(Int) -->
	chars_of_type(digit, Ds),
	{Ds = [_|_], number_codes(Int, Ds)}.

% opt_slash -->
% Esetleg kielemezheto egy `/' karakter.
opt_slash -->
	[0'/], !.
opt_slash --> [].

% layout_char(Dir, C):
% C egy Dir (horiz_sp|vert_sp) iranyu nem lathato karakter.
layout_char(horiz_sp, 0' ).
layout_char(horiz_sp, 0'\t).
layout_char(vert_sp,  0'\n).
layout_char(vert_sp,  0'\r).
layout_char(vert_sp,  0'\f).
layout_char(vert_sp,  0'\v).

% Kielemezheto egy Type tipusu C karakter.
char_of_type(Type, C) -->
	[C], {char_type(Type, C)}.

% chars_of_type(Type, Cs) -->
% Kielemezheto Type tipusu karakterek egy Cs listaja.
chars_of_type(Type, [C|Cs]) -->
	char_of_type(Type, C), !,
	chars_of_type(Type, Cs).
chars_of_type(_, []) --> [].

% char_type(Type, C) :
% A C karakter Type tipusu.
char_type(layout, C) :-
	layout_char(_, C).
char_type(digit, D) :-
	D >= 0'0, D =< 0'9.
char_type(mid_line, C) :-
	\+ layout_char(vert_sp, C).
char_type(Dir, C) :-
	layout_char(Dir, C).

%-------------- experimental ---------------------------
% Az al�bbi elj�r�st Eisenberger Andr�s �ltette �t Prologra Erlangb�l, �s eg�sz�tette ki a megold�s ellen�rz�s�vel.

:- use_module(library(file_systems)).
:- use_module(library(timeout)).
:- use_module(library(samsort)).

% A 'tests' k�nyvt�rban lev� �sszes "testXXXd.txt" teszt�llom�ny eset�n
%  - lefuttatja a tesztet Timeout m�sodperces id�korl�ttal,
%  - ellen�rzi, hogy a testXXXs.txt �llom�nyban megadott megold�shalmazt kapta,
%  - olvashat� form�ban (l�sd megold/2) ki�rja az eredm�nyt a 'tests_out_pl'
%    k�nyvt�r testXXXt.txt nev� �llom�ny�ba.
% Az �llom�nynevekben az XXX sz�m tetsz�leges hossz�s�g� lehet.
teljes_teszt(Timeout) :-
	Time_MS is Timeout * 1000,
	(   directory_exists(tests_out_pl) -> true
	;   make_directory(tests_out_pl)
	),
	file_members_of_directory('tests', 'test*d.txt', FoundFiles),
	(   member(BaseName-_AbsPath, FoundFiles),
	    atom_concat('tests/', BaseName, InPath),
	    atom_concat(TestName, 'd.txt', BaseName),
	    atom_concat('tests/', TestName, 's.txt', SolsPath),
	    atom_concat('tests_out_pl/', TestName, 't.txt', OutPath),
            time_out(stopper(InPath, OutPath, Sols),
		     Time_MS, Result),
            (   Result == success ->
		samsort(Sols, SolsSorted),
		catch(read_term_from_file(SolsPath, SolsRead), _,
		      SolsRead = none),
		(   SolsRead == none -> write(' NINCS MEGOLD�S\n')
		;   SolsRead == SolsSorted -> write(' HELYES\n')
		;   write(' NEM HELYES\n')
		)
	    ;	format('Feladvany: ~a, tullepte az idokorlatot (~d s)~n',
		       [InPath,Timeout])
            ),
	    flush_output,
	    fail
	;   true
	).

% atom_concat(A, B, C, ABC): Az A, B es C atomok �sszef�z�se ABC.
atom_concat(A, B, C, ABC) :-
	atom_concat(A, B, AB),
	atom_concat(AB, C, ABC).

% read_term_from_file(File, Term): A Term Prolog kifejez�st beolvassa a
% File �llom�nyb�l.
read_term_from_file(File, Term) :-
	open_file(File, read, Stream),
	call_cleanup(read(Stream, Term),
		     close(Stream)).
