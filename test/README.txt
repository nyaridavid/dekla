Tartalom:

kmmind.pl	Prolog keretprogram
kmmind.erl	Erlang keretprogram
p_idok.txt	Prolog futasi idők
e_idok.txt	Erlang futasi idők
README.txt	Leírás (ez a fájl)
tests/		Tesztesetek es megoldások

A futasi időket általunk rendkívül gyorsnak vélt algoritmusokkal
mértük, feladatonként 2 perces időkorláttal. Megjegyezzük, hogy
nem várunk el ilyen hatékonyságú házi feladatokat.
A mérést 32 bites Erlang és Prolog környezetben végeztük (az
alacsonyabb memóriaigény miatt) egy alábbi tipusú gépen:
model name  : Intel(R) Core(TM) i3-2120 CPU @ 3.30GHz

A méréshez az alábbi parancsok kimenetét használtuk:

ulimit -v 4000000

erl -noshell -eval 'kmmind:teljes_teszt(120).' -s init stop

sicstus --nologo --noinfo -l kmmind.pl --goal 'teljes_teszt(120),halt.'

Ahol a teljes_teszt(Timeout) eljárás/függvény a 'tests' könyvtárban levő
összes "testXXXd.txt" tesztállomány esetén
- lefuttatja a tesztet Timeout másodperces időkorláttal,
- ellenőrzi, hogy a testXXXs.txt állományban megadott megoldáshalmazt kapta
- olvasható formában kiírja az eredményt a 'tests_out'
  könyvtár testXXXt.txt nevű állományába.
Az állománynevekben az XXX szám tetszőleges hosszúságú lehet.


$LastChangedDate: 2015-10-28 09:53:59 +0100 (Wed, 28 Oct 2015) $
